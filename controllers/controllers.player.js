const db = require('../models');

exports.me = async (req,res) => {
    try {
    //cek di database
    const user = await db.Users.findAll({
        where:{
        token: req.header.authorization,
        }
    });
    //output jika matching
    if(user.length > 0){
        res.status(200).json({message: user});
    } 
    //output jika tidak matching
    else{
        res.status(401).json({message: "401 unauthorized"})
    }
    } catch (error) {
        //output jika tidak matching
        res.status(500).json({message: error})
    }
}