var express = require('express');
const passport = require('passport');
var router = express.Router();
const auth = require('../middleware/auth');
const playerController = require ('../controllers/controllers.player');

passport.use(auth.bearer);
const authCheck = passport.authenticate('bearer', {session:false});

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
//protected routes
router.get('/me', authCheck, playerController.me);

module.exports = router;
